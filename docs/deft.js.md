# deft.js
## Purpose?
Can be used for XHR(fetch), where `for (var ...)` is useless, ancient js friendly!<br>

## Example(s)
```js
// Start index=1
deft(1, 4, function (next, i, ub) {
	console.log("i:", i, "ubound:", ub);
	// reminder: this() === next()
	if (this()) console.log("Done iterating", i, ub);
});

// Start index=0
deft(0, 3, function (next, i, ub) {
	console.log("i:", i, "ubound:", ub);
	// reminder: this() === next()
	if (this()) console.log("Done iterating", i, ub);
});
```
