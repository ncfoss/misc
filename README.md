# Misc
Repository for anything, all content dedicated to public domain.<br>
<img src="https://api.visitorbadge.io/api/visitors?path=gitlab.ncfoss.misc&countColor=%2337d67a&style=flat-square&labelStyle=upper"><br>
## Content List
- <a href="https://gitlab.com/ncfoss/misc/-/blob/main/js/deft.js" target="_blank">deft.js</a>, usage: <a href="https://gitlab.com/ncfoss/misc/-/blob/main/docs/deft.js.md" target="_blank">deft.js docs</a><br>
- To be added later?
## License
This project is licensed under Unlicense, and is dedicated to public domain.<br>
